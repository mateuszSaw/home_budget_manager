package com.javagda19.home_budget.model;

public enum IncomeCategory {

    SALARY,
    RENTING,
    INVESTITION,
    OTHER
}
