package com.javagda19.home_budget.model;

public enum ExpenditureCategory {

    BILLS,
    RENTS,
    SHOPING,
    FOODSANDDRINKS,
    TRAVELS,
    TRANSPORTS,
    RESTAURANTS,
    HEALTH,
    FAMILY,
    LEISURE,


}
